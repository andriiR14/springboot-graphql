package org.andrii.sbgraphql.service;

import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
public class BalanceService {

    public Map<UUID, BigDecimal> getBalancerFor(Map<UUID, BankAccount> bankAccountMap, String userId) {
        log.info("Requesting balance for bank account ids [{}] for userId {}", bankAccountMap, userId);

        //Set<UUID> uuids = bankAccountMap.keySet();

        //retrieve bank accounts
        //validate ...

        Map<UUID, BigDecimal> result = new HashMap<>();
        result.put(UUID.fromString("935ec512-5bf9-4305-bded-729e725b8d10"), BigDecimal.ONE);
        result.put(UUID.fromString("4e081594-53b3-46b4-9ecd-df4109d12b4d"), BigDecimal.TEN);
        result.put(UUID.fromString("8ea1eac6-f84d-43a1-baef-5b661cccd24e"), BigDecimal.ZERO);
        return result;
    }
}
