package org.andrii.sbgraphql.util;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

@Component
@RequiredArgsConstructor
public class CorrelationIdPropagationExecutor implements Executor {

    private final Executor delegate;

    private static final String CORRELATION_ID = "correlation_id";

    public static Executor wrap(Executor executor) {
        return new CorrelationIdPropagationExecutor(executor);
    }

    @Override
    public void execute(@NotNull Runnable command) {
        String correlation_id = MDC.get(CORRELATION_ID);

        delegate.execute(() -> {
            try {
                MDC.put(CORRELATION_ID, correlation_id);
                command.run();
            } finally {
                MDC.remove(CORRELATION_ID);
            }

        });

    }
}
