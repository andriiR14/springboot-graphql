package org.andrii.sbgraphql.context.dataloader;

import lombok.RequiredArgsConstructor;
import org.andrii.sbgraphql.service.BalanceService;
import org.andrii.sbgraphql.util.CorrelationIdPropagationExecutor;
import org.dataloader.BatchLoaderEnvironment;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RequiredArgsConstructor
@Component
public class DataLoaderRegistryFactory {

    public static final String BALANCE_DATA_LOADER = "BALANCE_DATA_LOADER";

    private static final Executor balanceThreadPool = CorrelationIdPropagationExecutor.wrap(
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));

    @Autowired
    private BalanceService balanceService;

    public DataLoaderRegistry create(String userId) {

        DataLoaderRegistry registry = new DataLoaderRegistry();

        registry.register(BALANCE_DATA_LOADER, createBalanceDataLoader(userId));
        return registry;
    }

    private DataLoader<UUID, BigDecimal> createBalanceDataLoader(String userId) {
        return DataLoader.newMappedDataLoader((bankAccountMap, environment) ->
                CompletableFuture.supplyAsync(() ->
                                balanceService.getBalancerFor((Map)environment.getKeyContexts(), userId),
                        balanceThreadPool));
    }
}
