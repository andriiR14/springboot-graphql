package org.andrii.sbgraphql.resolver.bank;

import graphql.execution.DataFetcherResult;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.domain.bank.Asset;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@Slf4j
@Component
public class AssetResolver implements GraphQLResolver<BankAccount> {

    @Autowired
    private ExecutorService executorService;

    public CompletableFuture<DataFetcherResult<Asset>> asset(BankAccount bankAccount) {
        log.info("Getting asset of bank account with id: '{}'", bankAccount.getId());

        return CompletableFuture.supplyAsync(
                () -> DataFetcherResult.<Asset>newResult()
                        .data(Asset.builder()
                                .id(UUID.randomUUID())
                                .property("simple property")
                                .build())
                        .error(new GenericGraphQLError("Couldn't get asset"))
                        .build(),
                executorService);
    }
}
