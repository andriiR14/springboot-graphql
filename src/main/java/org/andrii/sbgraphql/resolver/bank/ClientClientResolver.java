package org.andrii.sbgraphql.resolver.bank;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.andrii.sbgraphql.domain.bank.Client;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class ClientClientResolver implements GraphQLResolver<Client> {

    public Client client(Client client) {
        log.info("Getting client of client with id: '{}'", client.getId());

        return Client.builder()
                .id(UUID.randomUUID())
                .firstName("Mat")
                .lastName("Bobson")
                .build();
    }
}
