package org.andrii.sbgraphql.resolver.bank.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.andrii.sbgraphql.domain.bank.input.BankAccountInput;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.UUID;

@Slf4j
@Component
@Validated
public class BankAccountMutation implements GraphQLMutationResolver {

    public BankAccount createBankAccount(@Valid BankAccountInput input) {
        log.info("Creating Bank account for {}", input);

        return BankAccount.builder()
                .id(UUID.randomUUID())
                .currency(input.getCurrency())
                .build();
    }
}
