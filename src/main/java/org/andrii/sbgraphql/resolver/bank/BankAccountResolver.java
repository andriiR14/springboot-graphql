package org.andrii.sbgraphql.resolver.bank;

import graphql.kickstart.tools.GraphQLResolver;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.context.dataloader.DataLoaderRegistryFactory;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class BankAccountResolver implements GraphQLResolver<BankAccount> {

    public CompletableFuture<BigDecimal> balance(BankAccount account, DataFetchingEnvironment environment) {

        log.info("Getting balance for bank account with id {}", account.getId());
        DataLoader<UUID, BigDecimal> dataLoader = environment.getDataLoader(DataLoaderRegistryFactory.BALANCE_DATA_LOADER);
        return dataLoader.load(account.getId(), account);
    }
}
