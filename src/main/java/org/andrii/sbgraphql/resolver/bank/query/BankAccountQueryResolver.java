package org.andrii.sbgraphql.resolver.bank.query;

import graphql.kickstart.tools.GraphQLQueryResolver;
import graphql.relay.*;
import graphql.schema.DataFetchingEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.connection.CursorUtil;
import org.andrii.sbgraphql.context.CustomGraphqlContext;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.andrii.sbgraphql.domain.bank.Currency;
import org.andrii.sbgraphql.repository.BankAccountRepository;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class BankAccountQueryResolver implements GraphQLQueryResolver {

    private final BankAccountRepository repository;
    private final CursorUtil cursorUtil;

    public BankAccount bankAccount(UUID id, DataFetchingEnvironment environment) {
        log.info("Retrieving bank account id: {}", id);

        CustomGraphqlContext context = environment.getContext();

        log.info("user_id {}", context.getUser_id());

        return BankAccount
                .builder()
                .id(id)
                .currency(Currency.USD)
                .createdOn(LocalDate.now())
                .createdAt(OffsetDateTime.now())
                .build();
    }

    public Connection<BankAccount> bankAccounts(Integer first, @Nullable String cursor) {

        List<Edge<BankAccount>> edges = getBankAccounts(cursor)
                .stream()
                .map(bankAccount -> new DefaultEdge<>(bankAccount, cursorUtil.createCursorWith(bankAccount.getId())))
                .limit(first)
                .collect(Collectors.toList());

        ConnectionCursor firstCursor = cursorUtil.getFirstCursorFrom(edges);
        ConnectionCursor lastCursor = cursorUtil.getLastCursorFrom(edges);

        DefaultPageInfo pageInfo = new DefaultPageInfo(firstCursor, lastCursor, cursor != null, edges.size() >= first);
        return new DefaultConnection<BankAccount>(edges, pageInfo);
    }

    private List<BankAccount> getBankAccounts(String cursor) {
        if (cursor == null) {
            return repository.getBankAccounts();
        }
        return repository.getBankAccountsAfter(cursorUtil.decode(cursor));
    }
}
