package org.andrii.sbgraphql.resolver.bank;

import graphql.execution.AbortExecutionException;
import graphql.execution.DataFetcherResult;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.andrii.sbgraphql.domain.bank.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@Slf4j
@Component
public class ClientResolver implements GraphQLResolver<BankAccount> {

    @Autowired
    private ExecutorService executorService;

    public CompletableFuture<DataFetcherResult<Client>> client(BankAccount bankAccount) {
        log.info("Getting client of bank account with id: '{}'", bankAccount.getId());

        return CompletableFuture.supplyAsync(
                () -> DataFetcherResult.<Client>newResult()
                        .data(Client.builder()
                                .id(UUID.randomUUID())
                                .firstName("Dan")
                                .lastName("Swano")
                                .build())
                        //.error(new GenericGraphQLError("Couldn't get client"))
                        .build(),
                executorService);
    }
}
