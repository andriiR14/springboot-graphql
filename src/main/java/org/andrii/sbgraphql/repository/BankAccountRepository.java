package org.andrii.sbgraphql.repository;

import org.andrii.sbgraphql.domain.bank.BankAccount;
import org.andrii.sbgraphql.domain.bank.Currency;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class BankAccountRepository {

    private List<BankAccount> bankAccounts;

    public BankAccountRepository() {

        List<UUID> uuids = new ArrayList<>();
        uuids.add(UUID.fromString("50ae3a41-cb59-4f8b-ab25-ede684da3e17"));
        uuids.add(UUID.fromString("b6ccf734-1f52-48be-bb48-ac8625f8515b"));
        uuids.add(UUID.fromString("935ec512-5bf9-4305-bded-729e725b8d10"));
        uuids.add(UUID.fromString("e459fc3d-dd20-49e4-ac43-aefd027a73db"));
        uuids.add(UUID.fromString("0c29e59b-efbd-44a1-8ae8-ac4b70968e55"));
        uuids.add(UUID.fromString("4e081594-53b3-46b4-9ecd-df4109d12b4d"));
        uuids.add(UUID.fromString("dcc53de3-406d-408f-819c-d71a63e2e573"));
        uuids.add(UUID.fromString("b08ca036-beba-4fcf-9eb8-964e2dc8c5e9"));
        uuids.add(UUID.fromString("8ea1eac6-f84d-43a1-baef-5b661cccd24e"));
        uuids.add(UUID.fromString("66e049be-fd65-4364-be8f-6167ecbf4fca"));
        bankAccounts = generateBankAccounts(uuids);
    }

    private List<BankAccount> generateBankAccounts(List<UUID> ids) {

        return ids.stream().map(uuid -> getBankAccount(uuid)).collect(Collectors.toList());

//        List<BankAccount> accounts = new ArrayList<>();
//        for (int i = 0; i < 11; i++) {
//            accounts.add(getBankAccount());
//        }
//        return accounts;
    }

    private BankAccount getBankAccount(UUID id) {
        return BankAccount.builder()
                .id(id)
                .currency(Currency.USD)
                .createdOn(LocalDate.now())
                .createdAt(OffsetDateTime.now())
                .build();
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public List<BankAccount> getBankAccountsAfter(UUID id) {
        Boolean flag = false;
        List<BankAccount> result = new ArrayList<>();
        for (BankAccount bankAccount: bankAccounts) {

            if (flag) {
                result.add(bankAccount);
            } else {
                if (bankAccount.getId().equals(id)) {
                    flag = true;
                    result.add(bankAccount);
                }
            }
        }
        return result;
    }
}
