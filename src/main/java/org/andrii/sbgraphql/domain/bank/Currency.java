package org.andrii.sbgraphql.domain.bank;

public enum Currency {
    USD,
    UAH
}
