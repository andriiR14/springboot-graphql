package org.andrii.sbgraphql.domain.bank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client {

    private UUID id;
    private String firstName;
    private String lastName;
    private Collection<String> middleNames;
    private Client client;
}
