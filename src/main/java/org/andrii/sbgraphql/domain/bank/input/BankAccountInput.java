package org.andrii.sbgraphql.domain.bank.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.andrii.sbgraphql.domain.bank.Currency;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankAccountInput {

    Currency currency;
    @NotNull
    Integer age;
    Integer number;
}
