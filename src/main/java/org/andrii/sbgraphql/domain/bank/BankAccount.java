package org.andrii.sbgraphql.domain.bank;


import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BankAccount {

    UUID id;
    Client client;
    Currency currency;
    Asset asset;
    LocalDate createdOn;
    OffsetDateTime createdAt;
    BigDecimal balance;
}
