package org.andrii.sbgraphql;

import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import io.micrometer.core.instrument.util.IOUtils;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.*;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {SpringbootGraphqlApplication.class}
)
public class BankAccountQueryResolverIT {

    private static final String GRAPHQL_QUERY_REQUEST_PATH = "/graphql_resolver/query/request/bank_account.graphql";
    private static final String GRAPHQL_QUERY_RESPONSE_PATH = "/graphql_resolver/query/response/bank_account.json";

    @Autowired
    private GraphQLTestTemplate graphQLTestTemplate;

    @Test
    public void bank_account_are_returned() throws IOException, JSONException {
        String testName = "bank+account";
        GraphQLResponse graphQLResponse = graphQLTestTemplate.postForResource(String.format(GRAPHQL_QUERY_REQUEST_PATH, testName));

        String expectedResponseBody = read(String.format(GRAPHQL_QUERY_RESPONSE_PATH, testName));

        assertThat(graphQLResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertEquals(expectedResponseBody, graphQLResponse.getRawResponse().getBody(), false);
    }

    private String read(String location) throws IOException {
        return IOUtils.toString(new ClassPathResource(location).getInputStream(), StandardCharsets.UTF_8);
    }
}
